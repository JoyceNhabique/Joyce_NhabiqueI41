package com.joyce_nhabiqueexample.equacaoquadratica.Adapters.RecycleView;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joyce_nhabiqueexample.equacaoquadratica.Model.HistoryModel;
import com.joyce_nhabiqueexample.equacaoquadratica.R;

import java.util.List;

/**
 * Created by Joyce_Nhabique on 11/6/2017.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.Holder> {

    private List<HistoryModel> histories;

    public HistoryAdapter(List<HistoryModel> histories) {
        this.histories = histories;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.layout_history_item, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        HistoryModel model = histories.get(position);
        holder.a.setText(model.getA()+"");
        holder.b.setText(model.getB()+"");
        holder.c.setText(model.getC()+"");
        holder.delta.setText(model.getDelta()+"");
        holder.x1.setText(model.getX1()+"");
        holder.x2.setText(model.getX2()+"");

    }

    @Override
    public int getItemCount() {
        return histories.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private TextView a, b, c, delta, x1, x2;

        public Holder(View itemView) {
            super(itemView);

            a = itemView.findViewById(R.id.a);
            b = itemView.findViewById(R.id.b);
            c = itemView.findViewById(R.id.c);
            delta = itemView.findViewById(R.id.delta);
            x1 = itemView.findViewById(R.id.x1);
            x2 = itemView.findViewById(R.id.x2);
        }
    }
}
