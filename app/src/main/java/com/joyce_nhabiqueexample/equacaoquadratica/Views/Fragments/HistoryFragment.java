package com.joyce_nhabiqueexample.equacaoquadratica.Views.Fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.joyce_nhabiqueexample.equacaoquadratica.Adapters.RecycleView.HistoryAdapter;
import com.joyce_nhabiqueexample.equacaoquadratica.R;
import com.joyce_nhabiqueexample.equacaoquadratica.ViewModel.MainViewModel;

/**
 * Created by Joyce_Nhabique on 11/6/2017.
 */

public class HistoryFragment extends Fragment {

    private RecyclerView rv_history;
    private MainViewModel viewModel;
    private HistoryAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rv_history = view.findViewById(R.id.rv_history);
        viewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);


    }

    @Override
    public void onResume() {
        super.onResume();

        adapter = new HistoryAdapter(viewModel.histories);
        rv_history.setAdapter(adapter);
        rv_history.setLayoutManager(new LinearLayoutManager(getContext()));
    }
}
