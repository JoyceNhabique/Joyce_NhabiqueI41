 package com.joyce_nhabiqueexample.equacaoquadratica.Views.Activity;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.joyce_nhabiqueexample.equacaoquadratica.Adapters.ViewPager.MainAdapter;
import com.joyce_nhabiqueexample.equacaoquadratica.R;

 public class MainActivity extends AppCompatActivity {

     private ViewPager vp_main;
     private TabLayout tab_main;
     private MainAdapter adapter;


     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         vp_main = findViewById(R.id.vp_main);
         tab_main = findViewById(R.id.tab_main);
         adapter = new MainAdapter(getSupportFragmentManager());

         vp_main.setAdapter(adapter);
         tab_main.setupWithViewPager(vp_main);
    }
}
