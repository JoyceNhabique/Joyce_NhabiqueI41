package com.joyce_nhabiqueexample.equacaoquadratica.ViewModel;

import android.arch.lifecycle.ViewModel;

import com.joyce_nhabiqueexample.equacaoquadratica.Model.HistoryModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joyce_Nhabique on 11/6/2017.
 */

public class MainViewModel extends ViewModel {

    public List<HistoryModel> histories = new ArrayList<>();
    public double delta(double a, double b, double c) {
        double delta;
        delta = Math.pow(b, 2) - (4 * a * c);
        return delta;
    }

    public void calculaRaizes(double a, double b, double c) {
        if (a != 0) {
            double delta = delta(a, b, c);
            double x1 = calculax1(a, b, c, delta);
            double x2 = calculax2(a, b, c, delta);
            histories.add(new HistoryModel(a, b, c, delta, x1, x2));
        }
    }

    public double calculax1(double a, double b, double c, double delta){
        return (-b + Math.sqrt(delta)) / (2 * a);
    }

    public double calculax2(double a, double b, double c, double delta){
        return (-b - Math.sqrt(delta)) / (2 * a);
    }
}

